/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include "Scene.hpp"

/// Gathers user input and displays text on-screen.
class UserInterface
{
public:
    /// Draws the (text-based) user interface on the screen.
    void draw( void );

    /// \name callbacks
    /// @{

    /// GLUT callback for a mouse event.
    /// \param button The button that changed state.
    /// \param state The state of the button that triggered the callback.
    /// \param x The horizontal coordinate of the mouse.
    /// \param y The vertical coordinate of the mouse.
    static void
    mouseEventCallback( int button, int state, int x, int y );

    /// GLUT callback for a mouse move event.
    /// \param x The new horizontal coordinate of the mouse.
    /// \param y The new vertical coordinate of the mouse.
    static void
    mouseMoveEventCallback( int x, int y );

    /// GLUT callback for a keyboard event.
    /// \param key The key that was pressed.
    /// \param x The horizontal coordinate of the mouse.
    /// \param y The vertical coordinate of the mouse.
    static void
    keyEventCallback( unsigned char key, int x, int y );

    /// @}



    /// Instantiates the object and sets a reference to the \ref Scene.
    /// \param scene The \ref Scene reference.
    UserInterface( Scene& scene );

private:
    /// \name typedefs
    /// @{

    /// A C-string to use with GLUT string rendering functions.
    using glut_string_t = unsigned char const *;

    /// @}



    /// \name mutators
    /// @{

    /// Updates the status string for the UI that displays which animation is
    /// runnning.
    /// \param status Pointer to the new status string.
    void
    updateAnimationStatus( glut_string_t const * status );

    /// Updates the status string for the UI that displays which skinning mode
    /// is being used.
    /// \param status Pointer to the new status string.
    void
    updateDquatStatus( glut_string_t const * status );

    /// Updates the status string for the UI that displays whether keyframe
    /// interpolation is enabled.
    /// \param status Pointer to the new status string.
    void
    updateInterpStatus( glut_string_t const * status );

    /// @}



    /// Implementation of the mouse event callback.
    void
    mouseEvent( int button, int state, int x, int y );

    /// Implementation of the mouse move event callback.
    void
    mouseMoveEvent( int x, int y );

    /// Implementation of the key event callback.
    void
    keyEvent( unsigned char key, int x, int y );



    /// \name attributes
    /// @{

    glut_string_t STR_DQUAT_ON{
        reinterpret_cast<glut_string_t>(
            "[q]        Skinning: Dual Quaternions."
        )
    };
    glut_string_t STR_DQUAT_OFF{
        reinterpret_cast<glut_string_t>(
            "[q]        Skinning: Linear Blending."
        )
    };
    glut_string_t STR_ANIMATION_REST{
        reinterpret_cast<glut_string_t>(
            "[a/s/d/f]  Animation: Rest Pose."
        )
    };
    glut_string_t STR_ANIMATION_WALK{
        reinterpret_cast<glut_string_t>(
            "[a/s/d/f]  Animation: Walking."
        )
    };
    glut_string_t STR_ANIMATION_RUN{
        reinterpret_cast<glut_string_t>(
            "[a/s/d/f]  Animation: Running."
        )
    };
    glut_string_t STR_ANIMATION_KF{
        reinterpret_cast<glut_string_t>(
            "[a/s/d/f]  Animation: Keyframed."
        )
    };
    glut_string_t STR_DRAW_SKEL_ON{
        reinterpret_cast<glut_string_t>(
            "[w]        Draw skeleton: on"
        )
    };
    glut_string_t STR_DRAW_SKEL_OFF{
        reinterpret_cast<glut_string_t>(
            "[w]        Draw skeleton: off"
        )
    };
    glut_string_t STR_INTERP_OFF{
        reinterpret_cast<glut_string_t>(
            "[e]        Interpolate keyframes: off"
        )
    };
    glut_string_t STR_INTERP_ON{
        reinterpret_cast<glut_string_t>(
            "[e]        Interpolate keyframes: on"
        )
    };
    glut_string_t STR_ADVANCE_FRAMES_ON{
        reinterpret_cast<glut_string_t>(
            "[spacebar] Auto-advance frames: on"
        )
    };
    glut_string_t STR_ADVANCE_FRAMES_OFF{
        reinterpret_cast<glut_string_t>(
            "[spacebar] Auto-advance frames: off"
        )
    };
    glut_string_t STR_FRAME_STEP{
        reinterpret_cast<glut_string_t>(
            "[tab]      Frame single-step."
        )
    };

    /// Pointer to the active status string for the skinning method.
    glut_string_t const * dQuatStatus_ = &STR_DQUAT_OFF;
    /// Pointer to the active status string for the active animation.
    glut_string_t const * animStatus_ = &STR_ANIMATION_REST;
    /// Pointer to the active status string for the keyframe interpolation.
    glut_string_t const * interpStatus_ = &STR_INTERP_OFF;

    // Reference to the scene. Needed to gather its state.
    Scene& scene_;
    /// The previous event's mouse coordinate on the X axis.
    int prevMouseX_ = -1;
    /// The previous event's mouse coordinate on the Y axis.
    int prevMouseY_ = -1;
    /// Flag for the left mouse button being pressed.
    bool mouseLmbPressed_ = false;
    /// Flag for the right mouse button being pressed.
    bool mouseRmbPressed_ = false;

    /// @}
};
