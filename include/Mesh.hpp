/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <vector>

#include "Vertex.hpp"

/// A set of triangles that correspond to one mesh object.
class Mesh
{
public:
    /// \name getters
    /// Getter member functions for the class.
    /// @{
    /// The number of vertices for the mesh.
    /// \return The number of vertices.
    std::size_t
    numVertices() const;

    /// The number of triangles for the mesh.
    /// \return The number of triangles.
    std::size_t
    numTriangles() const;

    /// Get the triangle index at the requested position.
    /// \pre \p i is in the range \f$[0, numTriangles)\f$.
    /// \param i The position of the triangle in the list.
    /// \return The triangle's index.
    int
    getIndex( std::size_t i ) const;

    /// Get the vertex at the requested position.
    /// \pre \p i is in the range \f$[0, numVertices)\f$.
    /// \param i The position of the \ref Vertex in the list.
    /// \return The vertex.
    Vertex const&
    getVertex( std::size_t i ) const;
    /// @}


    /// \name mutators
    /// Member functions for modifying the contents of the class.
    /// @{
    /// Add a vertex to the list of vertices.
    /// \param vertex The vertex to add.
    void
    addVertex( Vertex const& vertex );

    /// Add a triangle to the list of triangles. Each triangle is represented as
    /// a tuple of 3 \ref Vertex indices.
    /// \param index1 The first index.
    /// \param index2 The second index.
    /// \param index3 The third index.
    void
    addTriangle( int index1, int index2, int index3 );
    /// @}


    /// Draw the mesh at a given frame (or the interpolated result between two
    /// animation frames).
    /// \param currentFrame The frame to draw (or interpolate from, if enabled).
    /// \param nextFrame The frame to interpolate to, if enabled.
    /// \param interp The interpolation factor. Valid only in the range
    /// \f$[0,1]\f$. If interpolation is disabled, set to 0.
    void
    draw(std::size_t currentFrame, std::size_t nextFrame, float interp) const;

private:
    /// \name attributes
    /// @{

    /// All of the \ref Mesh vertices.
    std::vector<Vertex> vertices_;
    /// The triangles that compose the \ref Mesh. Given as indices into the
    /// \ref vertices_ std::vector.
    std::vector<int> triangles_;

    /// @}
};
