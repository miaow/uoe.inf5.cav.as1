/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <map>
#include <memory>

#include "Animation.hpp"
#include "Camera.hpp"
#include "Mesh.hpp"

/// Container for the \ref Mesh and \ref Animation objects in the scene and
/// options for drawing them on-screen.
class Scene
{
public:
    /// \name getters
    /// @{

    /// The scene's \ref Camera.
    /// \return The \ref Camera.
    Camera&
    getCamera() const;

    /// The scene's (only) character \ref Mesh.
    /// \return The \ref Mesh.
    Mesh const&
    getCharacter() const;

    /// The current animation's name.
    /// \return The name.
    std::string const&
    getCurrentAnimationName( void ) const;

    /// The current \ref Animation.
    /// \return The \ref Animation.
    Animation const&
    getCurrentAnimation( void ) const;

    /// Returns an animation corresponding to requested name.
    /// \pre \p animation must be a valid \ref Animation name.
    /// \param animation The {\ref Animation}'s name.
    /// \return The \ref Animation.
    Animation const&
    getAnimation( std::string const& animation ) const;

    /// The current animation frame number.
    /// \return The frame number.
    std::size_t
    getCurrentFrame( void );

    /// The next frame number for the animation. If the current frame is the
    /// last one for this animation, then returns the first frame (i.e. 0).
    /// \return The next frame number.
    std::size_t
    getNextFrame( void );

    /// The number of intermediate frames to interpolate.
    /// \return The number of interpolation frames.
    int
    getInterpFrames( void );

    /// Whether drawing the lines of the \ref Skeleton is enabled or not.
    /// \return true if enabled, false otherwise.
    bool
    isDrawSkeletonEnabled( void ) const;

    /// Whether automatic frame advance is enabled or not.
    /// \return true if enabled, false otherwise.
    bool
    isAutoAdvanceFrames( void );

    /// Whether dual quaternion skinning is enabled or not.
    /// \return true if enabled, false otherwise.
    bool
    isDualQuatSkinningEnabled( void );

    /// Whether keyframe interpolation is enabled or not. NB: keyframe
    /// interpolation is only valid when dual quaternion skinning is on.
    /// \return true if enabled, false otherwise.
    bool
    isKfInterpolationEnabled( void );

    /// @}



    /// \name mutators
    /// @{

    /// Changes the animation played back in the \ref Scene.
    /// \pre \p animation is a valid std::string key for an animation.
    /// \param animation The name of the animation to be played.
    void
    setCurrentAnimation(std::string animation);

    /// Resets the frame counter; this returns any animation back to its start.
    void
    resetFrameCounter( void );

    /// Advances one complete (animation) frame.
    void
    advanceFrame( void );

    /// Advances the interpolation frame counter.
    void
    advanceInterpFrame( void );

    /// Increases the number of intermediate frames to interpolate in between
    /// keyframes.
    void
    incFrameInterp( void );

    /// Decreases the number of intermediate frames to interpolate in between
    /// keyframes.
    void
    decFrameInterp( void );

    /// Toggles between linear blending skinning and dual quaternion skinning.
    bool
    toggleDualQuatSkinning( void );

    /// Toggles the drawing of the skeleton representation on or off.
    void
    toggleDrawSkeleton( void );

    /// Toggles on or off the automatic advance of the frame counter.
    void
    toggleAutoAdvanceFrames( void );

    /// Toggles keyframe interpolation on or off.
    /// NB: Keyframe interpolation is only valid for dual quaternion skinning.
    void
    toggleKfInterpolation( void );

    /// @}



    /// \name callbacks
    /// @{

    /// GLUT callback for redrawing the window contents.
    static void
    drawCallback( void );

    /// GLUT idle callback. Called when no events are registered. This allows
    /// continuous rendering.
    static void
    updateCallback( void );

    /// @}



    /// Initialises the scene. Loads all the model and animation data.
    void
    initialise();

private:

    /// Implementation of \ref drawCallback.
    void
    draw( void );

    /// Implementation of \ref updateCallback.
    void
    update( void );

    /// Loads a Source(tm) Engine model from the file system.
    /// \param filename The path to the model.
    /// \return A std::unique_ptr containing the \ref Mesh that is the model.
    std::unique_ptr<Mesh>
    loadSmdCharacter( std::string filename );

    /// Loads a Source(tm) Engine animation from the file system.
    /// \param filename The path to the animation.
    /// \return A std::unique_ptr containing the \ref Animation.
    std::unique_ptr<Animation>
    loadSmdAnimation( std::string filename );



    /// \name typedefs
    /// @{

    /// The animations' key type. Currently keyed by a string with the
    /// animation's name.
    using anim_keyType = std::string;
    /// The animations' value type. A std::unique_ptr holding the \ref Animation
    /// object.
    using anim_valType = std::unique_ptr<Animation>;

    /// State-tracker for the .smd file loaders (for both \ref Mesh and
    /// \ref Animation).
    enum
    {
        SMD_STATE_EMPTY = 0,
        SMD_STATE_MESH  = 1,
        SMD_STATE_NODES = 2,
        SMD_STATE_SKEL  = 3
    };

    /// @}



    /// \name attributes
    /// @{

    /// The scene's camera. Only one is allowed.
    std::unique_ptr<Camera>              camera_;
    /// The scene's \ref Mesh. Only one is needed.
    std::unique_ptr<Mesh>                character_;
    /// The set of all animations in the scene. Keyed by a unique name.
    std::map<anim_keyType, anim_valType> animations_;
    /// The current animation that is playing. Defaults to the rest pose.
    anim_keyType                         currentAnimation_            = "rest";
    /// The previous animation. Defaults to the rest pose.
    /// FIXME: animation blending not implemented.
    anim_keyType                         previousAnimation_           = "rest";
    /// The current animation's frame counter. Valid values are in the range
    /// \f$[0,numFrames)\f$.
    std::size_t                          frame_                       = 0;
    /// The current interpolation sub-frame. Valid values are in the range
    /// \f$[0,maxInterpFrames_{})\f$.
    int                                  interpFrame_                 = 0;
    /// The number of intermediate frames to interpolate between \ref Animation
    /// keyframes. Defaults to 5.
    int                                  maxInterpFrames_             = 5;
    /// Flag for enabling automatic frame advance. Defaults to false.
    bool                                 autoAdvanceFrames_           = false;
    /// Flag for enabling keyframe interpolation. Defaults to false.
    bool                                 enableKeyframeInterpolation_ = false;
    /// Flag for enabling the drawing of the skeleton. Defaults to false.
    bool                                 drawSkeleton_                = false;
    /// Flag for enabling dual quaternion skinning. Defaults to false.
    bool                                 dualQuatSkinningEnabled_     = false;

    /// @}
};
