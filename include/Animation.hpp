/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "Skeleton.hpp"

/// A collection of \ref Skeleton objects. Each \ref Skeleton object represents
/// a single frame of the animation.
class Animation
{
public:
    /// \name getters
    /// Getter member functions for the class.
    /// @{

    /// Non-const getter for a frame.
    /// \param i The frame number.
    /// \return The \ref Skeleton for the requested frame.
    /// \throws std::out_of_range If the frame number requested is
    /// out of bounds.
    Skeleton&
    getFrame( std::size_t i );

    /// Non-const getter for a frame.
    /// \param i The frame number.
    /// \return The \ref Skeleton for the requested frame.
    /// \throws std::out_of_range If the frame number requested is
    /// out of bounds.
    Skeleton const&
    getFrame( std::size_t i ) const;

    /// The number of frames in this animation.
    /// \return The number of frames.
    std::size_t
    numFrames() const;

    /// @}



    /// Add a frame to the animation.
    /// \param frame The \ref Skeleton for the frame.
    void
    addFrame( Skeleton const& frame );
    /// Draws the animation as a set of lines that represent the skeleton's
    /// pose.
    /// \param currentFrame The frame to draw (or interpolate from, if
    /// interpolation is enabled).
    /// \param nextFrame The frame to interpolate to.
    /// \param interp The interpolation factor. Valid only in the range
    /// \f$[0,1]\f$. If interpolation is disabled, set to 0.
    void
    draw( std::size_t currentFrame, std::size_t nextFrame, float interp ) const;

private:

    /// Draws the basis vectors for a matrix transform.
    /// \param origin The matrix transform. Assuming it is in the form:
    ///  | R t |
    ///  | 0 1 |
    /// Where R is a 3x3 rotation matrix, and t is a 3-element translation
    /// vector.
    void drawAxis( glm::mat4 const& origin ) const;

    /// \name attributes
    /// @{
    std::vector<Skeleton> frames_;
    /// @}
};
