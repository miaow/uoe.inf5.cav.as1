/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/dual_quaternion.hpp>

#include "Joint.hpp"

/// A hierarchy of \ref Joint objects.
class Skeleton
{
public:
    /// \name getters
    /// @{

    /// Calculates the coordinate space transformation matrix for a given joint.
    /// \pre \p i is in the range \f$[0, numJoints)\f$.
    /// \param i The index of the joint.
    /// \return a glm::mat4 transformation matrix.
    glm::mat4
    jointTransform( std::size_t i ) const;

    /// The number of joints in the \ref Skeleton.
    /// \return The number of joints.
    std::size_t
    numJoints() const;

    /// Non-const getter for the joint at index i.
    /// \pre \p i is in the range \f$[0, numJoints)\f$.
    /// \param i The index of the joint.
    /// \return The joint.
    Joint&
    getJoint( std::size_t i );

    /// Const getter for the joint at index i.
    /// \pre \p i is in the range \f$[0, numJoints)\f$.
    /// \param i The index of the joint.
    /// \return The joint.
    Joint const&
    getJoint( std::size_t i ) const;

    /// @}



    /// \name mutators
    /// @{

    /// Adds a joint to the \ref Skeleton.
    /// \param joint The joint to add.
    void
    addJoint( Joint const& joint );

    /// Updates the joint at a given index.
    /// \pre \p i is in the range \f$[0, numJoints)\f$.
    /// \param i The joint index.
    /// \param joint The new \ref Joint to place at that index.
    void
    setJoint( std::size_t i, Joint const& joint );

    /// @}

private:
    /// \name attributes
    /// @{

    /// Container for all the joints in the \ref Skeleton. Bones are implicit.
    std::vector<Joint> joints_;

    /// @}
};
