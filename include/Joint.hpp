/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <glm/glm.hpp>

/// Structure representing a single joint in the \ref Skeleton of a model.
struct Joint
{
    /// \name attributes
    /// @{

    /// The rotation of the joint. Rotations are applied as \f$YZX\f$.
    glm::mat4 rotation_;
    /// The position of the joint (homogeneous coordinates \f$(x,y,z,1)\f$).
    glm::vec4 position_;
    /// The joint identifier.
    int id_;
    /// The identifier of the parent joint. This link constitutes a bone.
    int parentId_;

    /// @}



    /// Constructor with id and parent id parameters.
    /// \param id The joint identifier.
    /// \param parentId The joint's parent identifier.
    Joint( int id = 0, int parentId = -1 );
};
