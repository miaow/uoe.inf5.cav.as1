/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

/// Represents the camera location and direction in the world.
class Camera
{
public:
    /// \name getters
    /// Getter member functions for the class.
    /// @{

    /// The current position of the camera.
    /// \return The position.
    glm::vec4 const&
    getPosition( void ) const;

    /// The current camera target.
    /// \return The target.
    glm::vec3 const&
    getTarget( void ) const;

    /// The current direction of the camera. Defined as the length of the vector
    /// \f$ target - position \f$.
    /// \return The direction.
    glm::vec3
    getDirection( void ) const;

    /// The field of view (FOV) of the camera.
    /// \return The FOV.
    float
    getFov( void ) const;

    /// The near clip plane of the camera.
    /// \return The near clip plane.
    float
    getNearClipPlane( void ) const;

    /// The far clip plane of the camera.
    /// \return The far clip plane.
    float
    getFarClipPlane( void ) const;

    /// The vector \f$ target - position \f$.
    /// \return The vector from the position to the target of the camera.
    glm::vec3
    positionToTarget( void ) const;

    /// @}



    /// \name setters
    /// Setter member functions for the class.
    /// @{

    /// Sets a new position for the camera.
    /// \param position The new position.
    void
    setPosition( glm::vec4 const& position );

    /// Sets a new target for the camera.
    /// \param target The new target.
    void
    setTarget( glm::vec3 const& target );

    /// Sets a new direction for the camera.
    /// \param direction The new direction.
    void
    setDirection( glm::vec3 const& direction );

    /// Sets a new field of view (FOV).
    /// \param fov The new FOV.
    void
    setFov( float fov );

    /// Sets a new near clip plane for the camera.
    /// \param nearClip The near clip plane.
    void
    setNearClipPlane( float nearClip );

    /// Sets a new far clip plane for the camera.
    /// \param farClip The far clip plane.
    void
    setFarClipPlane( float farClip );

    /// @}



    /// Constructor with user-specified position and target.
    /// \param position The position of the camera as a glm::vec4.
    /// \param target The camera's target as a glm::vec3.
    Camera(
        glm::vec4 const& position = glm::vec4(1.0f),
        glm::vec3 const& target = glm::vec3()
    );

private:
    /// \name attributes
    /// @{
    glm::vec4 position_;
    glm::vec3 target_;
    float fov_;
    float nearClip_;
    float farClip_;
    /// @}
};
