/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#pragma once

#include <map>

#include <glm/glm.hpp>

/// A point of the mesh.
/// Includes its position, normal, and the effects of each bone on it.
struct Vertex
{
    /// \name typedefs
    /// @{

    /// The identifier for a weight value. Corresponds to the bone id.
    using weightId  = int;

    /// The weight value as a float in the range \f$[0,1]\f$.
    using weightValue = float;

    /// @}



    /// The number of bones influencing this \ref Vertex.
    /// \return The number of weight factors.
    std::size_t
    numWeights( void );

    /// Add a weight with which a given bone influences the \ref Vertex
    /// position.
    /// \param id The bone identifier.
    /// \param value The weight value. Valid values are in the range
    /// \f$[0,1]\f$.
    void
    addWeight( weightId id, weightValue value );

    /// Constructor that sets the position and normal of the \ref Vertex.
    /// \param position The position in homogeneous coordinates.
    /// \param normal The vertex normal.
    Vertex(
        glm::vec4 const& position = glm::vec4(),
        glm::vec3 const& normal = glm::vec3()
    );



    /// \name attributes
    /// @{

    /// Set of all the bones (by id) that affect this vertex. Multiple entries
    /// per bone are allowed, hence the std::multimap.
    std::multimap<weightId, weightValue> weights_;

    /// The vertex position in homogeneous coordinates.
    glm::vec4 position_;

    /// The vertex normal.
    glm::vec3 normal_;

    /// @}
};
