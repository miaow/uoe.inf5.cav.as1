# Computer Animation and Visualisation Assessment 1 - Skinning


## Compilation

Run:

    make -j4


## Running

Run:

    LD_LIBRARY_PATH=../prebuilts/lib64 ./skinning


## Controls

* Click and drag left mouse button on screen to rotate camera
* Scroll mouse wheel to zoom
* Escape key to exit
* The rest of the controls are displayed on-screen in the application


## Features

* Minimum required feature set: skinning using linear blending, simple keyframe
  animation (press 'f' to display)
* Dual quaternion skinning (enable by pressing 'q')
* Keyframe blending (enable by pressing 'e'). NB: keyframe blending is only
  available when dual quaternion skinning is enabled. Otherwise, only the
  skeleton position is interpolated.
* Toggle-able skeleton display ('w' key)
* Toggle-able frame auto-advance (Spacebar)
* Frame single-step (by pressing Tab)

## Documentation

Source code documentation is available via Doxygen. Simply run:

    doxygen

in this file's directory. The HTML output is in `doc/html/index.html`.
