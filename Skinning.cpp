// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <memory>

#include <GL/freeglut.h>
#include <GL/glext.h>

#include "Scene.hpp"
#include "UserInterface.hpp"

std::unique_ptr<Scene> scene;
std::unique_ptr<UserInterface> ui;

int main( int argc, char** argv )
{
    // Initialise the scene and the UI in main() to avoid global init order
    // issues.
    scene = std::move( std::unique_ptr<Scene>( new Scene() ) );
    scene->initialise();

    ui = std::move(
        std::unique_ptr<UserInterface>( new UserInterface(*scene) )
    );

    // Initialise GLUT.
    glutInit( &argc, argv );
    glutInitDisplayMode(
        GLUT_RGB |
        GLUT_DOUBLE |
        GLUT_DEPTH |
        GLUT_MULTISAMPLE
    );

    // Set up the window.
    glutInitWindowSize( 800, 600 );
    glutCreateWindow( "Skinning" );

    // Set up the rendering method and the one and only light.
    glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );

    glEnable( GL_MULTISAMPLE );
    glEnable( GL_CULL_FACE );
    glShadeModel( GL_SMOOTH );

    GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat mat_shininess[] = { 100.0f };
    GLfloat light_position[] = { 1.0f, 1.0f, 1.0f, 0.0f };

    glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
    glMaterialfv( GL_FRONT, GL_SHININESS, mat_shininess );
    glLightfv( GL_LIGHT0, GL_POSITION, light_position );

    glEnable( GL_LIGHT0 );

    // Register the GLUT callbacks.
    glutDisplayFunc( Scene::drawCallback );
    glutIdleFunc( Scene::updateCallback );
    glutMouseFunc( UserInterface::mouseEventCallback );
    glutMotionFunc( UserInterface::mouseMoveEventCallback );
    glutKeyboardFunc( UserInterface::keyEventCallback );

    // And enter the main loop.
    glutMainLoop();

    return 0;
}
