/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <memory>

#include <GL/gl.h>

#include "Mesh.hpp"
#include "Scene.hpp"

std::size_t Mesh::numVertices() const
{
    return vertices_.size();
}

std::size_t Mesh::numTriangles() const
{
    return triangles_.size()/3;
}

int Mesh::getIndex( std::size_t i ) const
{
    return triangles_[i];
}

Vertex const& Mesh::getVertex( std::size_t i ) const
{
    return vertices_[i];
}

void Mesh::addVertex( const Vertex& vertex )
{
    vertices_.push_back(vertex);
}

void Mesh::addTriangle( int index1, int index2, int index3 )
{
    triangles_.push_back(index1);
    triangles_.push_back(index2);
    triangles_.push_back(index3);
}

// FIXME: This draw function still requires the scene.
extern std::unique_ptr<Scene> scene;
void Mesh::draw(
    std::size_t currentFrame,
    std::size_t nextFrame,
    float interp
) const
{
    // Prepare the data in a way the GL will like it.
    std::unique_ptr<float[]> worldPositions( new float[numVertices() * 3] );
    std::unique_ptr<float[]> worldNormals( new float[numVertices() * 3] );
    std::unique_ptr<int[]> triangles( new int[numTriangles() * 3] );

    // The joint transforms are used multiple times, so store them here to
    // reduce the computational workload.
    // TODO: Cache the joint transforms in the \ref Skeleton.
    std::vector<glm::mat4> restTransforms;
    std::vector<glm::mat4> animationTransforms;
    std::vector<glm::mat4> nextFrameTransforms;

    for(
        unsigned i = 0;
        i < scene->getAnimation("rest").getFrame(0).numJoints();
        ++i
    )
    {
        // Get the inverse joint transform for each joint in the rest pose.
        auto jtf = glm::inverse(
            scene->getAnimation("rest").getFrame(0).jointTransform(i)
        );
        restTransforms.push_back( jtf );
    }

    for(
        unsigned i = 0;
        i < scene->getCurrentAnimation().getFrame(currentFrame).numJoints();
        ++i
    )
    {
        // Get the joint transform for each joint in the current frame.
        auto jtf = scene->getCurrentAnimation()
                         .getFrame(currentFrame).jointTransform(i);
        animationTransforms.push_back( jtf );
    }

    for(
        unsigned i = 0;
        i < scene->getCurrentAnimation().getFrame(nextFrame).numJoints();
        ++i
    )
    {
        // Get the joint transform for each joint in the next frame.
        auto jtf = scene->getCurrentAnimation()
                         .getFrame(nextFrame).jointTransform(i);
        nextFrameTransforms.push_back( jtf );
    }

    // For each vertex, calculate its position and normal (for linear blending
    // skinning) and its dual quaternion (for DQ skinning).
    for( unsigned int i = 0; i < numVertices(); i++ )
    {
        glm::vec4 position;
        glm::vec3 normal;
        glm::dualquat dqAccumulator;
        // Zero-out the accumulator dualquat. The default constructor makes the
        // real part's w element 1.
        dqAccumulator.real.w = 0;

        auto const& vert = getVertex(i);

        for( auto it = vert.weights_.begin(); it != vert.weights_.end(); ++it )
        {
            // For each weight id, get the weight value and the associated
            // joint transform in rest pose and transform in the active
            // animation.
            glm::mat4 factor =
            animationTransforms[it->first] * restTransforms[it->first];

            glm::mat4 nextFrameFactor =
            nextFrameTransforms[it->first] * restTransforms[it->first];

            // glm::dualquat_cast requires a glm::mat3x4 in the form:
            // | R |
            // | t |
            // which, incidentally, is the first 3 columns of the transpose of
            // a homogeneous glm::mat4.
            glm::dualquat dqfactor =
            glm::dualquat_cast(glm::mat3x4(glm::transpose(factor)));
            glm::dualquat nextFrameDqfactor =
            glm::dualquat_cast(glm::mat3x4(glm::transpose(nextFrameFactor)));

            // Ensure that all dual quaternions have the same polarity. Invert
            // the non-conformists.
            // (this is a feature of dual quaternions, since the rotation around
            // the axis can be either along the short arc or the long one).
            if( glm::dot( glm::dualquat().real, dqfactor.real ) <= 0.f )
            {
                dqfactor *= -1.f;
            }

            if(
                glm::dot(
                    glm::dualquat().real,
                    nextFrameDqfactor.real
                ) <= 0.f
            )
            {
                nextFrameDqfactor *= -1.f;
            }

            // Use dual quaternion linear blending (DLB), i.e.
            // x(1-a) + yk, for k = -a if dot(x.real, y.real) < 0, a otherwise.
            // to interpolate the dual quaternions between the current and the
            // next keyframe.
            dqAccumulator = dqAccumulator
                + it->second * glm::lerp(dqfactor, nextFrameDqfactor, interp);

            // The linear blending skinning method does not get an interpolation
            // implementation. Boo.
            position +=  factor * vert.position_ * it->second;
            normal += glm::mat3(factor) * vert.normal_ * it->second;
        }

        // Normalise the accumulated dual quaternion, since it may have drifted
        // a bit.
        dqAccumulator = glm::normalize(dqAccumulator);

        // The rotation given by the dual quaternion is in its real part.
        glm::mat4 transformation = glm::mat4_cast(dqAccumulator.real);
        // The translation is extracted using the formula below:
        glm::fquat translation = 2.f * dqAccumulator.dual
            * glm::conjugate(dqAccumulator.real);
        // Combine the two in one homogeneous matrix...
        transformation[3][0] = translation.x;
        transformation[3][1] = translation.y;
        transformation[3][2] = translation.z;

        if( scene->isDualQuatSkinningEnabled() )
        {
            /// ... and apply it to the position and normal if DQ skinning is
            /// enabled.
            position = transformation * vert.position_;
            normal = glm::mat3(transformation) * vert.normal_;
        }

        // Finally, copy over the data to the GL arrays.
        worldPositions[( i * 3 ) + 0] = position.x;
        worldPositions[( i * 3 ) + 1] = position.y;
        worldPositions[( i * 3 ) + 2] = position.z;
        worldNormals[( i * 3 ) + 0] = normal.x;
        worldNormals[( i * 3 ) + 1] = normal.y;
        worldNormals[( i * 3 ) + 2] = normal.z;
    }

    for( unsigned int i = 0; i < numTriangles() * 3; i++ )
    {
        triangles[i] = getIndex( i );
    }

    glEnable( GL_DEPTH_TEST );
    glEnable( GL_LIGHTING );

    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_NORMAL_ARRAY );

    glVertexPointer( 3, GL_FLOAT, 0, worldPositions.get() );
    glNormalPointer( GL_FLOAT, 0, worldNormals.get() );

    glDrawElements(
        GL_TRIANGLES,
        static_cast<GLsizei>(numTriangles() * 3),
        GL_UNSIGNED_INT,
        triangles.get()
    );

    glDisableClientState( GL_VERTEX_ARRAY );
    glDisableClientState( GL_NORMAL_ARRAY );

    glDisable( GL_DEPTH_TEST );
    glDisable( GL_LIGHTING );
}
