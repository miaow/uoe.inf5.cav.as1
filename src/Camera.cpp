/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <glm/glm.hpp>

#include "Camera.hpp"

Camera::Camera( glm::vec4 const& position, glm::vec3 const& target )
    : position_( position )
    , target_( target )
    , fov_( 3.141f / 4.0f )
    , nearClip_( 8.0f )
    , farClip_( 128.0f )
{}


void Camera::setPosition( glm::vec4 const& position )
{
    position_ = position;
}

void Camera::setTarget( glm::vec3 const& target )
{
    target_ = target;
}

void Camera::setDirection( glm::vec3 const& direction )
{
    target_ = glm::normalize( glm::vec3(position_) + direction );
}

void Camera::setFov( float fov )
{
    fov_ = fov;
}

void Camera::setNearClipPlane( float nearClip )
{
    nearClip_ = nearClip;
}

void Camera::setFarClipPlane( float farClip )
{
    farClip_ = farClip;
}



glm::vec4 const& Camera::getPosition() const
{
    return position_;
}

glm::vec3 const& Camera::getTarget() const
{
    return target_;
}

glm::vec3 Camera::getDirection() const
{
    return glm::normalize( target_ - glm::vec3(position_) );
}

glm::vec3 Camera::positionToTarget() const
{
    return target_ - glm::vec3(position_);
}

float Camera::getFov() const
{
    return fov_;
}

float Camera::getNearClipPlane() const
{
    return nearClip_;
}

float Camera::getFarClipPlane() const
{
    return farClip_;
}
