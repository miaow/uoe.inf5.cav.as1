/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <cstdio>
#include <cstring>

#include <GL/gl.h>
#include <GL/freeglut.h>
#include <glm/gtc/matrix_transform.hpp>

#define GLUT_KEY_ESCAPE 27
#ifndef GLUT_WHEEL_UP
#   define GLUT_WHEEL_UP 3
#   define GLUT_WHEEL_DOWN 4
#endif

#include "UserInterface.hpp"

static UserInterface* ui;

UserInterface::UserInterface( Scene& scene )
: scene_(scene)
{
    ui = this;
}

void UserInterface::draw( void )
{
    glColor3f(0.f,0.f,0.f);

    // Get the current viewport coordinates:
    // viewport[0]: x
    // viewport[1]: y
    // viewport[2]: width
    // viewport[3]: height
    int viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    // Set up a 2D orthographic projection matrix, so that glRasterPos2d
    // coordinates correspond to pixel coordinates.
    //
    // +------------+
    // |^y          |
    // ||           |
    // |-->x        |
    // +------------+
    //
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(viewport[0], viewport[2], viewport[1], viewport[3], -1, 1);

    // Do not apply Model-View transformations to the text.
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    // Print text. Lines are listed from bottom to top.

    // Dual quaternion skinning.
    glRasterPos2d(32, 32);
    glutBitmapString(GLUT_BITMAP_9_BY_15, *dQuatStatus_);

    // Skeleton drawing.
    glRasterPos2d(32, 54);
    if( scene_.isDrawSkeletonEnabled() )
    {
        glutBitmapString(
            GLUT_BITMAP_9_BY_15,
            STR_DRAW_SKEL_ON
        );
    }
    else
    {
        glutBitmapString(
            GLUT_BITMAP_9_BY_15,
            STR_DRAW_SKEL_OFF
        );
    }

   // Keyframe interpolation.
    glRasterPos2d(32, 76);
    glutBitmapString(
        GLUT_BITMAP_9_BY_15,
        *interpStatus_
    );

    // Animation.
    glRasterPos2d(32, 98);
    glutBitmapString(GLUT_BITMAP_9_BY_15, *animStatus_);

    // Auto-advance frames.
    glRasterPos2d(32, 120);
    if( scene_.isAutoAdvanceFrames() )
    {
        glutBitmapString(
            GLUT_BITMAP_9_BY_15,
            STR_ADVANCE_FRAMES_ON
        );
    }
    else
    {
        glutBitmapString(
            GLUT_BITMAP_9_BY_15,
            STR_ADVANCE_FRAMES_OFF
        );
    }

    // Frame single-step.
    glRasterPos2d(32, 144);
    glutBitmapString(
        GLUT_BITMAP_9_BY_15,
        STR_FRAME_STEP
    );

    // Interpolation sub-frames.
    glRasterPos2d(32, 166);
    // Allocate some buffer for the interpolation speed string on the UI.
    unsigned char interpSpeed[128];
    std::memset(interpSpeed, 0, 128);
    std::sprintf(
        reinterpret_cast<char*>(interpSpeed),
        "[-/+]      Adjust interpolation speed: %d.",
        scene_.getInterpFrames()
    );
    glutBitmapString(
        GLUT_BITMAP_9_BY_15,
        interpSpeed
    );

    // Restore the old matrices.
    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
}

void UserInterface::mouseEventCallback(int button, int state, int x, int y)
{
    ui->mouseEvent(button, state, x, y);
}

void UserInterface::mouseMoveEventCallback(int x, int y)
{
    ui->mouseMoveEvent( x, y );
}

void UserInterface::keyEventCallback(unsigned char key, int x, int y)
{
    ui->keyEvent(key, x, y);
}



// private members

void UserInterface::updateDquatStatus( glut_string_t const * status )
{
    dQuatStatus_ = status;
}

void UserInterface::updateAnimationStatus( glut_string_t const * status )
{
    animStatus_ = status;
}

void UserInterface::updateInterpStatus( glut_string_t const * status )
{
    interpStatus_ = status;
}

void UserInterface::mouseEvent(int button, int state, int x, int y)
{
    auto& camera = scene_.getCamera();
    switch( button )
    {
    case GLUT_LEFT_BUTTON :
        if( state == GLUT_UP )
        {
            mouseLmbPressed_ = false;
        }

        if( state == GLUT_DOWN )
        {
            mouseLmbPressed_ = true;
            prevMouseX_ = x;
            prevMouseY_ = y;
        }

        break;

    case GLUT_RIGHT_BUTTON :
        if( state == GLUT_UP )
        {
            mouseRmbPressed_ = false;
        }

        if( state == GLUT_DOWN )
        {
            mouseRmbPressed_ = true;
            prevMouseX_ = x;
            prevMouseY_ = y;
        }

        break;

    case GLUT_WHEEL_UP:
        camera.setPosition(
            camera.getPosition() + glm::vec4(camera.getDirection(), 0.f)
        );
        break;

    case GLUT_WHEEL_DOWN:
        camera.setPosition(
            camera.getPosition() - glm::vec4(camera.getDirection(), 0.f)
        );
        break;

    default:
        break;
    }
}

void UserInterface::mouseMoveEvent(int x, int y)
{
    if( mouseLmbPressed_ )
    {
        auto& camera = scene_.getCamera();
        int diffX = x - prevMouseX_;
        int diffY = y - prevMouseY_;

        // Make a copy of the current target.
        auto offset = camera.getTarget();

        camera.setTarget( camera.getTarget() - offset );
        camera.setPosition( camera.getPosition() - glm::vec4(offset, 0.f) );

        camera.setPosition(
            glm::rotate(
                glm::mat4(),
                0.01f * static_cast<float>(-diffX),
                glm::vec3{0.f,1.f,0.f}
            ) * camera.getPosition()
        );

        glm::vec3 axis =
        glm::normalize(
            glm::cross(glm::vec3{0.f,1.f,0.f}, camera.getDirection() )
        );
        glm::vec4 newPosition = glm::rotate(
            glm::mat4(),
            0.01f * static_cast<float>(diffY),
            axis
        ) * camera.getPosition();
        if( newPosition.x * camera.getPosition().x < 0.f &&
            newPosition.z * camera.getPosition().z < 0.f
        )
        {
            // The camera has changed quadrants (i.e. its azimuth angle has gone
            // beyond 90 degrees). Therefore, cancel the rotation.
            newPosition = camera.getPosition();
        }
        camera.setPosition( newPosition );

        camera.setTarget( camera.getTarget() + offset );
        camera.setPosition( camera.getPosition() + glm::vec4(offset, 0.f) );

        prevMouseX_ = x;
        prevMouseY_ = y;
    }
}

void UserInterface::keyEvent(unsigned char key, int x, int y)
{
    // Unused parameters:
    (void)x;
    (void)y;
    bool dqEnabled, interp;
    switch( key )
    {
        case GLUT_KEY_ESCAPE:
            glutLeaveMainLoop();
            break;
        // a/s/d/f chooses the active animation.
        case 'a':
            scene_.setCurrentAnimation("rest");
            scene_.resetFrameCounter();
            updateAnimationStatus(&STR_ANIMATION_REST);
            break;
        case 's':
            scene_.setCurrentAnimation("walk");
            scene_.resetFrameCounter();
            updateAnimationStatus(&STR_ANIMATION_WALK);
            break;
        case 'd':
            scene_.setCurrentAnimation("run");
            scene_.resetFrameCounter();
            updateAnimationStatus(&STR_ANIMATION_RUN);
            break;
        case 'f':
            scene_.setCurrentAnimation("kf");
            scene_.resetFrameCounter();
            updateAnimationStatus(&STR_ANIMATION_KF);
            break;
        // q toggles dual quaternion skinning.
        case 'q':
            dqEnabled = scene_.toggleDualQuatSkinning();
            if( dqEnabled )
            {
                updateDquatStatus(&STR_DQUAT_ON);
            }
            else
            {
                updateDquatStatus(&STR_DQUAT_OFF);
            }
            break;
        // w toggles the drawing of the skeleton.
        case 'w':
            scene_.toggleDrawSkeleton();
            break;
        // e toggles Keyframe interpolation.
        case 'e':
            scene_.toggleKfInterpolation();
            interp = scene_.isKfInterpolationEnabled();
            if( interp )
            {
                updateInterpStatus(&STR_INTERP_ON);
            }
            else
            {
                updateInterpStatus(&STR_INTERP_OFF);
            }
            break;
        // spacebar toggles frame auto-advance.
        case ' ':
            scene_.toggleAutoAdvanceFrames();
            break;
        // +/- alter the amount of interpolated frames.
        case '+':
            scene_.incFrameInterp();
            break;
        case '-':
            scene_.decFrameInterp();
            break;
        // tab advances one (interpolated) frame.
        case '\t':
            if( scene_.isKfInterpolationEnabled() )
            {
                scene_.advanceInterpFrame();
            }
            else
            {
                scene_.advanceFrame();
            }
            break;
        default:
            break;
    }
}
