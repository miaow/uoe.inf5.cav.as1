/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <stdexcept>

#include <GL/gl.h>

#include <glm/gtx/dual_quaternion.hpp>

#include "Animation.hpp"

void Animation::addFrame( Skeleton const& frame )
{
    frames_.push_back(frame);
}

Skeleton& Animation::getFrame( std::size_t i )
{
    // Scott Meyers says this is okay. I tend to believe him.
    return const_cast<Skeleton&>(
        const_cast<Animation const&>(*this).getFrame(i)
    );
}

Skeleton const& Animation::getFrame( std::size_t i ) const
{
    if( i >= frames_.size() )
    {
        throw std::out_of_range("Frame index out of range");
    }
    return frames_[i];
}

std::size_t Animation::numFrames() const
{
    return frames_.size();
}

void Animation::draw(
    std::size_t currentFrame,
    std::size_t nextFrame,
    float interp
) const
{
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    glLineWidth(2.0f);

    glBegin(GL_LINES);

    // Get both this frame's and next frame's skeletal position so that we can
    // interpolate between the two.
    auto& skeleton = getFrame(currentFrame);
    auto& nextSkeleton = getFrame(nextFrame);

    for (std::size_t i = 0; i < skeleton.numJoints(); i++) {
        int bone_id = static_cast<int>(i);
        int parent_id = skeleton.getJoint(i).parentId_;

        if (parent_id == -1) continue;

        // Use dual quaternions to interpolate the skeleton position
        // (simpler that way, plus arithmetic mixing using glm::mix leads to
        // the wrong results, e.g. arms shortening).

        glm::dualquat currentBone = glm::dualquat_cast(
            glm::mat3x4(glm::transpose(skeleton.jointTransform(bone_id)))
        );
        glm::dualquat currentParent = glm::dualquat_cast(
            glm::mat3x4(glm::transpose(skeleton.jointTransform(parent_id)))
        );

        glm::dualquat nextBone = glm::dualquat_cast(
            glm::mat3x4(glm::transpose(nextSkeleton.jointTransform(bone_id)))
        );
        glm::dualquat nextParent = glm::dualquat_cast(
            glm::mat3x4(glm::transpose(nextSkeleton.jointTransform(parent_id)))
        );

        // The last column of the glm::mat4 is the translation vector.
        glm::vec4 bone_pos = glm::mat4(
            glm::transpose(glm::mat3x4_cast(
                glm::lerp(currentBone, nextBone, interp))
            )
        )[3];
        glm::vec4 parent_pos = glm::mat4(
            glm::transpose(glm::mat3x4_cast(
                glm::lerp(currentParent, nextParent, interp))
            )
        )[3];

        glVertex3f(bone_pos.x, bone_pos.y, bone_pos.z);
        glVertex3f(parent_pos.x, parent_pos.y, parent_pos.z);
    }

    glEnd();

    glLineWidth(1.0f);
    glColor4f(1.0, 1.0, 1.0, 1.0);

    for(std::size_t i = 0; i < skeleton.numJoints(); i++) {
        // same goes for interpolating the axes. This time use the whole matrix
        // since we need the rotation info as well.
        drawAxis(glm::mat4(glm::transpose(
            glm::mat3x4_cast(glm::lerp(
                glm::dualquat_cast(glm::mat3x4(
                    glm::transpose(skeleton.jointTransform(i)))
                ),
                glm::dualquat_cast(glm::mat3x4(
                    glm::transpose(nextSkeleton.jointTransform(i)))
                ),
                interp)
            )
        )));
    }
}

void Animation::drawAxis(const glm::mat4& origin) const
{
    float const size = 0.5f;

    // Get the origin of the basis vectors in the world space. This is simply
    // the translation part of the origin matrix.
    glm::vec4 center = origin[3];
    // And then each basis vector's direction, scaled to be visible on-screen.
    glm::vec3 axis_x = glm::vec3(origin * glm::vec4(size,0.f,0.f,1.f));
    glm::vec3 axis_y = glm::vec3(origin * glm::vec4(0.f,size,0.f,1.f));
    glm::vec3 axis_z = glm::vec3(origin * glm::vec4(0.f,0.f,size,1.f));

    glLineWidth(2.0f);
    glBegin(GL_LINES);
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex3f(center.x, center.y, center.z);
    glVertex3f(axis_x.x, axis_x.y, axis_x.z);

    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f(center.x, center.y, center.z);
    glVertex3f(axis_y.x, axis_y.y, axis_y.z);

    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    glVertex3f(center.x, center.y, center.z);
    glVertex3f(axis_z.x, axis_z.y, axis_z.z);
    glEnd();
    glLineWidth(1.0f);
}
