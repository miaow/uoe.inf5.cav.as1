/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <glm/gtc/matrix_transform.hpp>

#include "Skeleton.hpp"

std::size_t Skeleton::numJoints() const
{
    return joints_.size();
}

Joint& Skeleton::getJoint( std::size_t i )
{
    return const_cast<Joint&>(const_cast<Skeleton const&>(*this).getJoint(i));
}

Joint const& Skeleton::getJoint( std::size_t i ) const
{
    return joints_[i];
}

glm::mat4 Skeleton::jointTransform( std::size_t i ) const
{
    glm::mat4 ret;
    auto const& joint = getJoint(i);
    // Rotate, then translate.
    ret = glm::translate(
        glm::mat4(1.0f),
        glm::vec3(joint.position_)
    ) * joint.rotation_;
    if( joint.parentId_ == -1 )
    {
        // This is the root, so the transform is only this joint's matrix.
        return ret;
    }
    // Recursively apply the transforms in the joint chain until you get to the
    // root.
    // TODO: cache these results.
    return jointTransform(static_cast<std::size_t>(joint.parentId_)) * ret;
}

void Skeleton::addJoint( Joint const& joint )
{
    joints_.push_back(joint);
}

void Skeleton::setJoint( std::size_t i, Joint const& joint )
{
    joints_[i] = joint;
}
