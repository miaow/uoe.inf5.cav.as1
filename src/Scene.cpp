/* Copyright (c) 2015, Mihail Atanassov
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;

#include <cstring>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <GL/freeglut.h>
#include <glm/gtx/euler_angles.hpp>

#include "Scene.hpp"
#include "UserInterface.hpp"

// Thank GLUT for this abomination. Should consider SDL for the future.
Scene* scenePtr = nullptr;

void Scene::initialise()
{
    // Place the camera in a decent location.
    camera_ = std::move(
        std::unique_ptr<Camera>(
            new Camera(
                glm::vec4( 20.f, 30.f, 50.f, 1.f ),
                glm::vec3( 0.f, 15.f, 0.f )
            )
        )
    );
    // Load all resources.
    character_ = std::move( loadSmdCharacter( "./resources/character.smd" ) );
    animations_.emplace(
        "rest",
        std::move(
            std::unique_ptr<Animation>(
                loadSmdAnimation( "./resources/rest_animation.smd" )
            )
        )
    );
    animations_.emplace(
        "walk",
        std::move(
            std::unique_ptr<Animation>(
                loadSmdAnimation( "./resources/walk_animation.smd" )
            )
        )
    );
    animations_.emplace(
        "run",
        std::move(
            std::unique_ptr<Animation>(
                loadSmdAnimation( "./resources/run_animation.smd" )
            )
        )
    );
    animations_.emplace(
        "kf",
        std::move(
            std::unique_ptr<Animation>(
                loadSmdAnimation( "./resources/kf_animation.smd" )
            )
        )
    );
    // Obviously, there should be only one \ref Scene, ever.
    scenePtr = this;
}

Camera& Scene::getCamera() const
{
    return *camera_;
}

Mesh const& Scene::getCharacter() const
{
    return *character_;
}

const std::string& Scene::getCurrentAnimationName(void) const
{
    return currentAnimation_;
}

const Animation& Scene::getCurrentAnimation( void ) const
{
    return *animations_.at(currentAnimation_);
}

const Animation& Scene::getAnimation( std::string const& animation ) const
{
    if( animations_.find(animation) == animations_.end() )
    {
        throw std::out_of_range("Could not find animation");
    }
    return *animations_.at(animation);
}

std::size_t Scene::getCurrentFrame(void)
{
    return frame_;
}

std::size_t Scene::getNextFrame( void )
{
    return (frame_+1)%getCurrentAnimation().numFrames();
}

int Scene::getInterpFrames(void)
{
    return maxInterpFrames_;
}

bool Scene::isDrawSkeletonEnabled( void ) const
{
    return drawSkeleton_;
}

bool Scene::isAutoAdvanceFrames(void)
{
    return autoAdvanceFrames_;
}

bool Scene::isDualQuatSkinningEnabled()
{
    return dualQuatSkinningEnabled_;
}

bool Scene::isKfInterpolationEnabled( void )
{
    return enableKeyframeInterpolation_;
}



void Scene::setCurrentAnimation( std::string animation )
{
    if( animations_.find(animation) == animations_.end() )
    {
        throw std::out_of_range("Could not find animation");
    }
    previousAnimation_ = currentAnimation_;
    currentAnimation_ = animation;
}

void Scene::resetFrameCounter( void )
{
    frame_ = 0;
}

void Scene::advanceFrame( void )
{
    ++frame_ %= getCurrentAnimation().numFrames();
}

void Scene::advanceInterpFrame( void )
{
    ++interpFrame_ %= maxInterpFrames_;
    if( interpFrame_ == 0)
    {
        advanceFrame();
    }
}

void Scene::incFrameInterp(void)
{
    maxInterpFrames_++;
    // Limit the interpolated frames to 25.
    if( maxInterpFrames_ > 25 )
        maxInterpFrames_ = 25;
}

void Scene::decFrameInterp(void)
{
    maxInterpFrames_--;
    // Since interpolated frames run in the range [0,maxInterpFrames_), 1 is the
    // minimum allowed.
    if( maxInterpFrames_ < 1 )
        maxInterpFrames_ = 1;
}

bool Scene::toggleDualQuatSkinning( void )
{
    return dualQuatSkinningEnabled_ ^= 1;
}

void Scene::toggleDrawSkeleton( void )
{
    drawSkeleton_ ^= 1;
}

void Scene::toggleAutoAdvanceFrames( void )
{
    autoAdvanceFrames_ ^=1 ;
}

void Scene::toggleKfInterpolation(void)
{
    enableKeyframeInterpolation_ ^= 1;
    if( !enableKeyframeInterpolation_ )
    {
        // Return the interpolated frame counter to the start, so that the
        // animation is in the right place.
        interpFrame_ = 0;
    }
}




void Scene::drawCallback(void)
{
    // This is the only sane way to not cause an explosion of staticness.
    scenePtr->draw();
}

void Scene::updateCallback(void)
{
    scenePtr->update();
}



// Private members.

void Scene::draw(void)
{
    const int WIDTH = 800;
    const int HEIGHT = 600;
    auto& camera = getCamera();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective( 40.0, ( float ) WIDTH / ( float ) HEIGHT, 1.0, 1000.0 );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    gluLookAt(
        camera.getPosition().x, camera.getPosition().y, camera.getPosition().z,
        camera.getTarget().x, camera.getTarget().y, camera.getTarget().z,
        0.0, 1.0, 0.0
    );

    // Advance (interpolated) frames, if enabled.
    if( isAutoAdvanceFrames() )
    {
        if( isKfInterpolationEnabled() )
        {
            advanceInterpFrame();
        }
        else
        {
            advanceFrame();
        }
    }

    // Draw the character.
    getCharacter().draw(
        getCurrentFrame(),
        getNextFrame(),
        static_cast<float>(interpFrame_)/static_cast<float>(maxInterpFrames_) );

    // Draw the skeleton, if enabled.
    if( isDrawSkeletonEnabled() )
    {
        getCurrentAnimation().draw(
            getCurrentFrame(),
            getNextFrame(),
            static_cast<float>(interpFrame_)
            /static_cast<float>(maxInterpFrames_)
        );
    }

    // Draw the UI.
    extern std::unique_ptr<UserInterface> ui;
    ui->draw();

    // Done. It's that simple.
    glutSwapBuffers();
}

void Scene::update(void)
{
    glutPostRedisplay();
}

std::unique_ptr<Mesh> Scene::loadSmdCharacter( std::string filename )
{
    int state = SMD_STATE_EMPTY;

    std::vector<Vertex> verts;
    std::vector<int> tris;

    // std::ifstream closes the file automatically when it goes out of scope.
    std::ifstream f( filename.c_str() );

    if( f == nullptr )
    {
        std::printf( "Failed to read file %s\n", filename.c_str() );
        std::fflush( stdout );
        exit( EXIT_FAILURE );
    }

    while( !f.eof() )
    {
        std::string line;
        std::getline( f, line );

        if( line.find("end") != line.npos )
        {
            state = SMD_STATE_EMPTY;
            continue;
        }

        if( line.find("triangles") != line.npos )
        {
            state = SMD_STATE_MESH;
            continue;
        }

        if( state == SMD_STATE_MESH )
        {
            int vertId = 0;
            float x, y, z, nx, ny, nz, u, v;
            int num_links = 0;
            int lastChar = 0;


            if( std::sscanf(
                line.c_str(),
                "%i %f %f %f %f %f %f %f %f %i%n",
                &vertId,
                &x, &y, &z,
                &nx, &ny, &nz,
                &u, &v,
                &num_links,
                &lastChar
            ) >= 10 )
            {
                Vertex vert;
                // Due to a mess-up in the export format, swap y & z.
                vert.position_ = glm::vec4( x, z, y, 1.f );
                vert.normal_ = glm::vec3( nx, nz, ny );
                // Get the substring starting after the number of links.
                std::stringstream ss( line.substr(lastChar) );
                for( int i = 0; i < num_links; ++i )
                {
                    // Add each link (id-value pair of weights) to the vertex.
                    int id;
                    float value;
                    ss >> id >> value;
                    vert.addWeight(id, value);
                }
                verts.push_back( vert );
                tris.push_back( static_cast<int>( verts.size() -1 ) );
            }
        }
    }

    // Create a \ref Mesh and add all the vertices and triangles to it.
    std::unique_ptr<Mesh> mesh(new Mesh());
    for( auto& vert : verts )
    {
        mesh->addVertex(vert);
    }
    for( auto index = tris.begin(); index != tris.end(); index += 3 )
    {
        mesh->addTriangle(*(index+2), *(index+1), *(index));
    }
    return std::move(mesh);
}

std::unique_ptr<Animation> Scene::loadSmdAnimation( std::string filename )
{
    int state = SMD_STATE_EMPTY;

    Skeleton base;
    std::unique_ptr<Animation> anim(new Animation());

    std::vector<Joint> joints;

    std::ifstream f( filename.c_str() );

    if( f == nullptr )
    {
        std::printf( "Failed to read file %s\n", filename.c_str() );
        std::fflush( stdout );
        exit( EXIT_FAILURE );
    }

    while( !f.eof() )
    {
        std::string line;
        std::getline( f, line );

        if( line.find( "end" ) != line.npos )
        {
            state = SMD_STATE_EMPTY;
            continue;
        }

        if( line.find( "nodes" ) != line.npos )
        {
            state = SMD_STATE_NODES;
            continue;
        }

        if( line.find( "skeleton" ) != line.npos )
        {
            state = SMD_STATE_SKEL;
            for( auto& it : joints )
            {
                base.addJoint( it );
            }
            continue;
        }

        if( line.find( "time" ) != line.npos )
        {
            anim->addFrame( base );
        }

        if( state == SMD_STATE_NODES )
        {
            char name[256];
            int id, parent;

            if(
                std::sscanf(
                    line.c_str(),
                    "%i \"%[^\"]\" %i",
                    &id,
                    name,
                    &parent
                ) == 3
            )
            {
                joints.emplace_back( id, parent );
            }
        }

        if( state == SMD_STATE_SKEL )
        {
            std::size_t id;
            float x, y, z, rx, ry, rz;

            if(
                std::sscanf(
                    line.c_str(),
                    "%lu %f %f %f %f %f %f",
                    &id,
                    &x, &y, &z,
                    &rx, &ry, &rz
                ) == 7
            )
            {
                auto& frame = anim->getFrame( anim->numFrames() - 1 );

                /* Swap y and z */
                frame.getJoint(id).position_ =
                    glm::vec4( x, z, y, 1.f );
                // The rotations are applied in the order YZX. They are also all
                // inverted in sign.
                frame.getJoint(id).rotation_ =
                    glm::eulerAngleYZ(-rz, -ry) *
                    glm::eulerAngleX(-rx);
            }
        }
    }

    return std::move(anim);
}
