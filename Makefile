CXX=g++-4.9

INCS= -I ./include -isystem ./thirdparty/glm
LIBS= -L ./lib -L ./

#-Wfloat-equal \
#-Wpadded \
#-Wsign-conversion \

CXXFLAGS= $(INCS) \
    -std=c++11 \
    -Wall \
    -Wextra \
    -Wundef \
    -Wunused \
    -Wuninitialized \
    -Wpedantic \
    -Wdouble-promotion \
    -Wswitch-default \
    -Wswitch-enum \
    -fstrict-aliasing \
    -Wstrict-overflow=5 \
    -Wtrampolines \
    -Wshadow \
    -Wcast-qual \
    -Wconversion \
    -Wzero-as-null-pointer-constant \
    -Wlogical-op \
    -Wmissing-declarations \
    -Wmissing-field-initializers \
    -Wpacked \
    -Wredundant-decls \
    -Winline \
    -Wvector-operation-performance \
    -O3 \
    -g

CXXFLAGS += -isystem ../thirdparty/glm

CPP_FILES= $(wildcard src/*.cpp)
OBJ_FILES= $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))

ifeq ($(findstring MINGW,$(shell uname)),MINGW)
	LFLAGS = $(LIBS) -lglut -lglu32 -lopengl32
endif

# -pthread added as workaround for linker bug introduced in
# commit 879707c642925947e156b7ae2169b89f844532cd for ld.so
ifeq ($(findstring Linux,$(shell uname)),Linux)
	LFLAGS = $(LIBS) -lglut -lGLU -lGL -pthread
endif

skinning: $(OBJ_FILES) Skinning.cpp
	$(CXX) Skinning.cpp $(OBJ_FILES) $(CXXFLAGS) $(LFLAGS) -o skinning

obj/%.o: src/%.cpp | obj
	$(CXX) $< -c $(CXXFLAGS) -o $@

obj:
	mkdir obj

clean:
	rm -f $(OBJ_FILES)
